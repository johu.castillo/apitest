# Laravel Rest API Mundo Telecomunicaciones

## Ejecutar el proyecto luego de clonar
### `composer install`
### `cp .env.example .env`
### `php artisan key:generate`
### `php artisan migrate --seed`
### `php artisan serve`

Tener en cuenta el nombre de la base de datos en el archivo .env

A continuación, tenemos una secuencia de solicitudes que se utilizarán para probar la API.
## Obtiene la lista de las bodegas registradas

```
GET api/bodegas/get-bodegas
200 OK
```

## Obtiene la lista de las marcas registradas

```
GET api/marcas/get-marcas 
200 OK
```
## Obtiene la lista de los modelos registradas por ID de marca

```
GET api/modelos/get-modelos/:marca_id
200 OK
```

## Obtiene la lista de los dispositivos por marca

```
POST api/dispositivos/disp-marca  {"marca_id":"1"}
200 OK
```

## Obtiene la lista de los dispositivos por modelo

```
POST api/dispositivos/disp-modelo  {"modelo_id":"1"}
200 OK
```

## Obtiene la lista de los dispositivos por ID de bodega

```
GET api/dispositivos/disp-bodega/:bodega_id
200 OK
```

## Obtiene la lista de los dispositivos de forma genérica (es posible obtener resultados por bodega, marca y/o modelo)

```
POST api/dispositivos/get  {"bodegaId": "","marcaId": "1","modeloId": "1"}
200 OK
```

En el proyecto está incluído el diagrama Entidad-Relación de la Base de datos, por el cual se pudieron crear las relaciones de los modelos. El nombre del archivo es: ERDDiagramTest.jpg
