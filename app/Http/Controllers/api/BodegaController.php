<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bodega;

class BodegaController extends Controller
{
    /**
    * @api {get} /bodegas/get-bodegas Obtiene la lista de las bodegas registradas
    * @apiName getBodegas
    *
    * @apiSuccessExample Success-Response:
    *     HTTP/1.1 200 OK
    *    {
    *       "success": true,
    *       "content": [
    *           {
    *               "id": 1,
    *               "nombre": "Prof. Lew Bernier",
    *               "created_at": "2022-08-25T22:37:13.000000Z",
    *               "updated_at": "2022-08-25T22:37:13.000000Z"
    *           },
    *           {
    *               "id": 2,
    *               "nombre": "Dan Schmidt Jr.",
    *               "created_at": "2022-08-25T22:37:13.000000Z",
    *               "updated_at": "2022-08-25T22:37:13.000000Z"
    *           },
    *           {
    *               "id": 3,
    *               "nombre": "Kirsten Moen",
    *               "created_at": "2022-08-25T22:37:13.000000Z",
    *               "updated_at": "2022-08-25T22:37:13.000000Z"
    *           },
    *           {
    *               "id": 4,
    *               "nombre": "Jennie Stiedemann",
    *               "created_at": "2022-08-25T22:37:13.000000Z",
    *               "updated_at": "2022-08-25T22:37:13.000000Z"
    *           },
    *           {
    *               "id": 5,
    *               "nombre": "Prof. Woodrow Keeling",
    *               "created_at": "2022-08-25T22:37:13.000000Z",
    *               "updated_at": "2022-08-25T22:37:13.000000Z"
    *           },
    *           {
    *               "id": 6,
    *               "nombre": "Wendell Treutel",
    *               "created_at": "2022-08-25T22:37:13.000000Z",
    *               "updated_at": "2022-08-25T22:37:13.000000Z"
    *           },
    *           {
    *               "id": 7,
    *               "nombre": "Mr. Macey Hilpert DDS",
    *               "created_at": "2022-08-25T22:37:13.000000Z",
    *               "updated_at": "2022-08-25T22:37:13.000000Z"
    *           },
    *           {
    *               "id": 8,
    *               "nombre": "Jan Ondricka",
    *               "created_at": "2022-08-25T22:37:13.000000Z",
    *               "updated_at": "2022-08-25T22:37:13.000000Z"
    *           },
    *           {
    *               "id": 9,
    *               "nombre": "Bessie Hahn III",
    *               "created_at": "2022-08-25T22:37:13.000000Z",
    *               "updated_at": "2022-08-25T22:37:13.000000Z"
    *           },
    *           {
    *               "id": 10,
    *               "nombre": "Mrs. Rosalind Walter Sr.",
    *               "created_at": "2022-08-25T22:37:13.000000Z",
    *               "updated_at": "2022-08-25T22:37:13.000000Z"
    *           }
    *       ]
    *   }
    */
    public function getBodegas()
    {
        try {
            $bodegas = Bodega::all();
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => 'Ocurrió un error al mostar la lista de las bodegas.',
                'content' => $th->getMessage()
            ], 500);
        }

        return response()->json([
            'success' => true,
            'content' => $bodegas
        ], 200);
    }
}
