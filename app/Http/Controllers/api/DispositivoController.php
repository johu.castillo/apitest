<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Dispositivo;
use App\Models\Marca;
use App\Models\Bodega;

class DispositivoController extends Controller
{
    /**
    * @api {post} /dispositivos/disp-marca Obtiene la lista de los dispositivos por marca
    * @apiName getDispositivosByMarca
    * @apiGroup dispositivos
    *
    * @apiParam {Number} marca_id ID de la marca.
    *
    * @apiSuccessExample Success-Response:
    *     HTTP/1.1 200 OK
    *    {
    *       "success": true,
    *       "content": [
    *           {
    *               "id": 1,
    *               "nombre": "Samsung",
    *               "created_at": "2022-08-25T22:37:14.000000Z",
    *               "updated_at": "2022-08-25T22:37:14.000000Z",
    *               "dispositivos": [
    *                   {
    *                       "id": 1,
    *                       "sku": "kmfZxQ0w",
    *                       "nombre": "Teléfono",
    *                       "modelo_id": 1,
    *                       "created_at": "2022-08-25T22:37:14.000000Z",
    *                       "updated_at": "2022-08-25T22:37:14.000000Z",
    *                       "laravel_through_key": 1,
    *                       "modelo": {
    *                           "id": 1,
    *                           "nombre": "Galaxy S22",
    *                           "marca_id": 1,
    *                           "created_at": "2022-08-25T22:37:14.000000Z",
    *                           "updated_at": "2022-08-25T22:37:14.000000Z",
    *                           "marca": {
    *                               "id": 1,
    *                               "nombre": "Samsung",
    *                               "created_at": "2022-08-25T22:37:14.000000Z",
    *                               "updated_at": "2022-08-25T22:37:14.000000Z"
    *                           }
    *                       }
    *                   },
    *                   {
    *                       "id": 2,
    *                       "sku": "98MVkDsz",
    *                       "nombre": "Teléfono",
    *                       "modelo_id": 2,
    *                       "created_at": "2022-08-25T22:37:14.000000Z",
    *                       "updated_at": "2022-08-25T22:37:14.000000Z",
    *                       "laravel_through_key": 1,
    *                       "modelo": {
    *                           "id": 2,
    *                           "nombre": "Galaxy S20",
    *                           "marca_id": 1,
    *                           "created_at": "2022-08-25T22:37:14.000000Z",
    *                           "updated_at": "2022-08-25T22:37:14.000000Z",
    *                           "marca": {
    *                               "id": 1,
    *                               "nombre": "Samsung",
    *                               "created_at": "2022-08-25T22:37:14.000000Z",
    *                               "updated_at": "2022-08-25T22:37:14.000000Z"
    *                           }
    *                       }
    *                   },
    *                   {
    *                       "id": 3,
    *                       "sku": "eGEjH5P5",
    *                       "nombre": "Teléfono",
    *                       "modelo_id": 3,
    *                       "created_at": "2022-08-25T22:37:14.000000Z",
    *                       "updated_at": "2022-08-25T22:37:14.000000Z",
    *                       "laravel_through_key": 1,
    *                       "modelo": {
    *                           "id": 3,
    *                           "nombre": "Galaxy A21s",
    *                           "marca_id": 1,
    *                           "created_at": "2022-08-25T22:37:14.000000Z",
    *                           "updated_at": "2022-08-25T22:37:14.000000Z",
    *                           "marca": {
    *                               "id": 1,
    *                               "nombre": "Samsung",
    *                               "created_at": "2022-08-25T22:37:14.000000Z",
    *                               "updated_at": "2022-08-25T22:37:14.000000Z"
    *                           }
    *                       }
    *                   },
    *                   {
    *                       "id": 4,
    *                       "sku": "Mla7T9wz",
    *                       "nombre": "Teléfono",
    *                       "modelo_id": 4,
    *                       "created_at": "2022-08-25T22:37:14.000000Z",
    *                       "updated_at": "2022-08-25T22:37:14.000000Z",
    *                       "laravel_through_key": 1,
    *                       "modelo": {
    *                           "id": 4,
    *                           "nombre": "Galaxy Note 10+",
    *                           "marca_id": 1,
    *                           "created_at": "2022-08-25T22:37:14.000000Z",
    *                           "updated_at": "2022-08-25T22:37:14.000000Z",
    *                           "marca": {
    *                               "id": 1,
    *                               "nombre": "Samsung",
    *                               "created_at": "2022-08-25T22:37:14.000000Z",
    *                               "updated_at": "2022-08-25T22:37:14.000000Z"
    *                           }
    *                       }
    *                   }
    *               ]
    *           }
    *       ]
    *    }
    */
    public function getDispositivosByMarca(Request $request)
    {
        try {
            $dispositivos = Marca::with('dispositivos', 'dispositivos.modelo')->where('id', $request->marca_id)->get();
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => 'Ocurrió un error al listar los dispositivos por marca.',
                'content' => $th->getMessage()
            ], 500);
        }

        return response()->json([
            'success' => true,
            'content' => $dispositivos
        ], 200);
    }


    /**
    * @api {post} /dispositivos/get Obtiene la lista de los dispositivos de forma genérica
    * @apiName getDispositivos
    * @apiGroup dispositivos
    *
    * @apiParam {Number} bodegaId ID de la bodega.
    * @apiParam {Number} marcaId ID de la marca.
    * @apiParam {Number} modeloId ID del modelo.
    *
    * @apiSuccessExample Success-Response:
    *     HTTP/1.1 200 OK
    *    {
    *       "success": true,
    *       "content": [
    *           {
    *               "id": 1,
    *               "sku": "kmfZxQ0w",
    *               "nombre": "Teléfono",
    *               "modelo_id": 1,
    *               "created_at": "2022-08-25T22:37:14.000000Z",
    *               "updated_at": "2022-08-25T22:37:14.000000Z",
    *               "modelo": {
    *                   "id": 1,
    *                   "nombre": "Galaxy S22",
    *                   "marca_id": 1,
    *                   "created_at": "2022-08-25T22:37:14.000000Z",
    *                   "updated_at": "2022-08-25T22:37:14.000000Z",
    *                   "marca": {
    *                       "id": 1,
    *                       "nombre": "Samsung",
    *                       "created_at": "2022-08-25T22:37:14.000000Z",
    *                       "updated_at": "2022-08-25T22:37:14.000000Z"
    *                   }
    *               },
    *               "bodegas": [
    *                   {
    *                       "id": 1,
    *                       "nombre": "Prof. Lew Bernier",
    *                       "created_at": "2022-08-25T22:37:13.000000Z",
    *                       "updated_at": "2022-08-25T22:37:13.000000Z",
    *                       "cantidad": {
    *                           "dispositivo_id": 1,
    *                           "bodega_id": 1,
    *                           "cantidad": 41
    *                       }
    *                   },
    *                   {
    *                       "id": 4,
    *                       "nombre": "Jennie Stiedemann",
    *                       "created_at": "2022-08-25T22:37:13.000000Z",
    *                       "updated_at": "2022-08-25T22:37:13.000000Z",
    *                       "cantidad": {
    *                           "dispositivo_id": 1,
    *                           "bodega_id": 4,
    *                           "cantidad": 21
    *                       }
    *                   },
    *                   {
    *                       "id": 6,
    *                       "nombre": "Wendell Treutel",
    *                       "created_at": "2022-08-25T22:37:13.000000Z",
    *                       "updated_at": "2022-08-25T22:37:13.000000Z",
    *                       "cantidad": {
    *                           "dispositivo_id": 1,
    *                           "bodega_id": 6,
    *                           "cantidad": 85
    *                       }
    *                   },
    *                   {
    *                       "id": 8,
    *                       "nombre": "Jan Ondricka",
    *                       "created_at": "2022-08-25T22:37:13.000000Z",
    *                       "updated_at": "2022-08-25T22:37:13.000000Z",
    *                       "cantidad": {
    *                           "dispositivo_id": 1,
    *                           "bodega_id": 8,
    *                           "cantidad": 133
    *                       }
    *                   },
    *                   {
    *                       "id": 9,
    *                       "nombre": "Bessie Hahn III",
    *                       "created_at": "2022-08-25T22:37:13.000000Z",
    *                       "updated_at": "2022-08-25T22:37:13.000000Z",
    *                       "cantidad": {
    *                           "dispositivo_id": 1,
    *                           "bodega_id": 9,
    *                           "cantidad": 107
    *                       }
    *                   }
    *               ]
    *           }
    *       ]
    *    }
    */
    public function getDispositivos(Request $request)
    {
        $bodegaId = $request->bodegaId ?? null;
        $marcaId = $request->marcaId ?? null;
        $modeloId = $request->modeloId ?? null;

        try {
            $dispositivo = new Dispositivo;

            if ($modeloId) {
                $dispositivo = $dispositivo->where('modelo_id', '=', $modeloId);
            }

            if ($bodegaId) {
                $dispositivo = $dispositivo->whereHas('bodegas', function ($query) use($bodegaId) {
                    return $query->where('bodega_id', '=', $bodegaId);
                });
            }

            if (!$modeloId && $marcaId) {
                $dispositivo = $dispositivo->whereHas('modelo.marca', function ($query) use($marcaId) {
                    return $query->where('id', '=', $marcaId);
                });
            }

            return response()->json([
                'success' => true,
                'content' => $dispositivo->with('modelo', 'bodegas')->get()
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => 'Ocurrió un error al listar los dispositivos por marca.',
                'content' => $th->getMessage()
            ], 500);
        }
    }


    /**
    * @api {post} /dispositivos/disp-modelo Obtiene la lista de los dispositivos por modelo
    * @apiName getDispositivosByModelo
    * @apiGroup dispositivos
    *
    * @apiParam {Number} modelo_id ID del modelo.
    *
    * @apiSuccessExample Success-Response:
    *     HTTP/1.1 200 OK
    *    {
    *       "success": true,
    *       "content": [
    *           {
    *               "id": 2,
    *               "sku": "98MVkDsz",
    *               "nombre": "Teléfono",
    *               "modelo_id": 2,
    *               "created_at": "2022-08-25T22:37:14.000000Z",
    *               "updated_at": "2022-08-25T22:37:14.000000Z",
    *               "modelo": {
    *                   "id": 2,
    *                   "nombre": "Galaxy S20",
    *                   "marca_id": 1,
    *                   "created_at": "2022-08-25T22:37:14.000000Z",
    *                   "updated_at": "2022-08-25T22:37:14.000000Z",
    *                   "marca": {
    *                       "id": 1,
    *                       "nombre": "Samsung",
    *                       "created_at": "2022-08-25T22:37:14.000000Z",
    *                       "updated_at": "2022-08-25T22:37:14.000000Z"
    *                   }
    *               }
    *           }
    *       ]
    *    }
    */
    public function getDispositivoByModelo(Request $request)
    {
        try {
            $dispositivos = Dispositivo::with('modelo', 'modelo.marca')->where('modelo_id', $request->modelo_id)->get();
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => 'Ocurrió un error al listar los dispositivos por modelo.',
                'content' => $th->getMessage()
            ], 500);
        }

        return response()->json([
            'success' => true,
            'content' => $dispositivos
        ], 200);
    }


     /**
    * @api {get} /dispositivos/disp-bodega/:id Obtiene la lista de los dispositivos por ID de bodega
    * @apiName getDispositivoByBodega
    * @apiGroup dispositivos
    *
    * @apiSuccessExample Success-Response:
    *     HTTP/1.1 200 OK
    *    {
    *       "success": true,
    *       "content": {
    *           "id": 2,
    *           "nombre": "Dan Schmidt Jr.",
    *           "created_at": "2022-08-25T22:37:13.000000Z",
    *           "updated_at": "2022-08-25T22:37:13.000000Z",
    *           "dispositivos": [
    *               {
    *                   "id": 3,
    *                   "sku": "eGEjH5P5",
    *                   "nombre": "Teléfono",
    *                   "modelo_id": 3,
    *                   "created_at": "2022-08-25T22:37:14.000000Z",
    *                   "updated_at": "2022-08-25T22:37:14.000000Z",
    *                   "cantidad": {
    *                       "bodega_id": 2,
    *                       "dispositivo_id": 3,
    *                       "cantidad": 142
    *                   },
    *                   "modelo": {
    *                       "id": 3,
    *                       "nombre": "Galaxy A21s",
    *                       "marca_id": 1,
    *                       "created_at": "2022-08-25T22:37:14.000000Z",
    *                       "updated_at": "2022-08-25T22:37:14.000000Z",
    *                       "marca": {
    *                           "id": 1,
    *                           "nombre": "Samsung",
    *                           "created_at": "2022-08-25T22:37:14.000000Z",
    *                           "updated_at": "2022-08-25T22:37:14.000000Z"
    *                       }
    *                   }
    *               },
    *               {
    *                   "id": 12,
    *                   "sku": "BMqKeBDi",
    *                   "nombre": "Teléfono",
    *                   "modelo_id": 12,
    *                   "created_at": "2022-08-25T22:37:14.000000Z",
    *                   "updated_at": "2022-08-25T22:37:14.000000Z",
    *                   "cantidad": {
    *                       "bodega_id": 2,
    *                       "dispositivo_id": 12,
    *                       "cantidad": 58
    *                   },
    *                   "modelo": {
    *                       "id": 12,
    *                       "nombre": "moto G22",
    *                       "marca_id": 3,
    *                       "created_at": "2022-08-25T22:37:14.000000Z",
    *                       "updated_at": "2022-08-25T22:37:14.000000Z",
    *                       "marca": {
    *                           "id": 3,
    *                           "nombre": "Motorola",
    *                           "created_at": "2022-08-25T22:37:14.000000Z",
    *                           "updated_at": "2022-08-25T22:37:14.000000Z"
    *                       }
    *                   }
    *               },
    *               {
    *                   "id": 13,
    *                   "sku": "2ROyS37R",
    *                   "nombre": "Teléfono",
    *                   "modelo_id": 13,
    *                   "created_at": "2022-08-25T22:37:14.000000Z",
    *                   "updated_at": "2022-08-25T22:37:14.000000Z",
    *                   "cantidad": {
    *                       "bodega_id": 2,
    *                       "dispositivo_id": 13,
    *                       "cantidad": 129
    *                   },
    *                   "modelo": {
    *                       "id": 13,
    *                       "nombre": "808 PureView",
    *                       "marca_id": 4,
    *                       "created_at": "2022-08-25T22:37:14.000000Z",
    *                       "updated_at": "2022-08-25T22:37:14.000000Z",
    *                       "marca": {
    *                           "id": 4,
    *                           "nombre": "Nokia",
    *                           "created_at": "2022-08-25T22:37:14.000000Z",
    *                           "updated_at": "2022-08-25T22:37:14.000000Z"
    *                       }
    *                   }
    *               },
    *               {
    *                   "id": 16,
    *                   "sku": "7utHPXxX",
    *                   "nombre": "Teléfono",
    *                   "modelo_id": 16,
    *                   "created_at": "2022-08-25T22:37:14.000000Z",
    *                   "updated_at": "2022-08-25T22:37:14.000000Z",
    *                   "cantidad": {
    *                       "bodega_id": 2,
    *                       "dispositivo_id": 16,
    *                       "cantidad": 140
    *                   },
    *                   "modelo": {
    *                       "id": 16,
    *                       "nombre": "7 Plus",
    *                       "marca_id": 4,
    *                       "created_at": "2022-08-25T22:37:14.000000Z",
    *                       "updated_at": "2022-08-25T22:37:14.000000Z",
    *                       "marca": {
    *                           "id": 4,
    *                           "nombre": "Nokia",
    *                           "created_at": "2022-08-25T22:37:14.000000Z",
    *                           "updated_at": "2022-08-25T22:37:14.000000Z"
    *                       }
    *                   }
    *               },
    *               {
    *                   "id": 8,
    *                   "sku": "pxx5Jjdp",
    *                   "nombre": "Teléfono",
    *                   "modelo_id": 8,
    *                   "created_at": "2022-08-25T22:37:14.000000Z",
    *                   "updated_at": "2022-08-25T22:37:14.000000Z",
    *                   "cantidad": {
    *                       "bodega_id": 2,
    *                       "dispositivo_id": 8,
    *                       "cantidad": 48
    *                   },
    *                   "modelo": {
    *                       "id": 8,
    *                       "nombre": "Redmi Note 11",
    *                       "marca_id": 2,
    *                       "created_at": "2022-08-25T22:37:14.000000Z",
    *                       "updated_at": "2022-08-25T22:37:14.000000Z",
    *                       "marca": {
    *                           "id": 2,
    *                           "nombre": "Xiaomi",
    *                           "created_at": "2022-08-25T22:37:14.000000Z",
    *                           "updated_at": "2022-08-25T22:37:14.000000Z"
    *                       }
    *                   }
    *               },
    *               {
    *                   "id": 4,
    *                   "sku": "Mla7T9wz",
    *                   "nombre": "Teléfono",
    *                   "modelo_id": 4,
    *                   "created_at": "2022-08-25T22:37:14.000000Z",
    *                   "updated_at": "2022-08-25T22:37:14.000000Z",
    *                   "cantidad": {
    *                       "bodega_id": 2,
    *                       "dispositivo_id": 4,
    *                       "cantidad": 72
    *                   },
    *                   "modelo": {
    *                       "id": 4,
    *                       "nombre": "Galaxy Note 10+",
    *                       "marca_id": 1,
    *                       "created_at": "2022-08-25T22:37:14.000000Z",
    *                       "updated_at": "2022-08-25T22:37:14.000000Z",
    *                       "marca": {
    *                           "id": 1,
    *                           "nombre": "Samsung",
    *                           "created_at": "2022-08-25T22:37:14.000000Z",
    *                           "updated_at": "2022-08-25T22:37:14.000000Z"
    *                       }
    *                   }
    *               },
    *               {
    *                   "id": 13,
    *                   "sku": "2ROyS37R",
    *                   "nombre": "Teléfono",
    *                   "modelo_id": 13,
    *                   "created_at": "2022-08-25T22:37:14.000000Z",
    *                   "updated_at": "2022-08-25T22:37:14.000000Z",
    *                   "cantidad": {
    *                       "bodega_id": 2,
    *                       "dispositivo_id": 13,
    *                       "cantidad": 105
    *                   },
    *                   "modelo": {
    *                       "id": 13,
    *                       "nombre": "808 PureView",
    *                       "marca_id": 4,
    *                       "created_at": "2022-08-25T22:37:14.000000Z",
    *                       "updated_at": "2022-08-25T22:37:14.000000Z",
    *                       "marca": {
    *                           "id": 4,
    *                           "nombre": "Nokia",
    *                           "created_at": "2022-08-25T22:37:14.000000Z",
    *                           "updated_at": "2022-08-25T22:37:14.000000Z"
    *                       }
    *                   }
    *               }
    *           ]
    *       }
    *    }
    */
    public function getDispositivoByBodega(Request $request)
    {
        try {
            $dispositivos = Bodega::with('dispositivos', 'dispositivos.modelo')->findOrFail($request->bodega_id);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => 'Ocurrió un error al listar los dispositivos por modelo.',
                'content' => $th->getMessage()
            ], 500);
        }

        return response()->json([
            'success' => true,
            'content' => $dispositivos
        ], 200);
    }
}
