<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Modelo;

class ModeloController extends Controller
{
    /**
    * @api {get} /modelos/get-modelos/:id Obtiene la lista de los modelos registradas por ID de marca
    * @apiName getModelos
    *
    * @apiSuccessExample Success-Response:
    *     HTTP/1.1 200 OK
    *    {
    *       "success": true,
    *       "content": [
    *          {
    *               "id": 1,
    *               "nombre": "Galaxy S22",
    *               "marca_id": 1,
    *               "created_at": "2022-08-25T22:37:14.000000Z",
    *               "updated_at": "2022-08-25T22:37:14.000000Z",
    *               "marca": {
    *                   "id": 1,
    *                   "nombre": "Samsung",
    *                   "created_at": "2022-08-25T22:37:14.000000Z",
    *                   "updated_at": "2022-08-25T22:37:14.000000Z"
    *               }
    *           },
    *           {
    *               "id": 2,
    *               "nombre": "Galaxy S20",
    *               "marca_id": 1,
    *               "created_at": "2022-08-25T22:37:14.000000Z",
    *               "updated_at": "2022-08-25T22:37:14.000000Z",
    *               "marca": {
    *                   "id": 1,
    *                   "nombre": "Samsung",
    *                   "created_at": "2022-08-25T22:37:14.000000Z",
    *                   "updated_at": "2022-08-25T22:37:14.000000Z"
    *               }
    *           },
    *           {
    *               "id": 3,
    *               "nombre": "Galaxy A21s",
    *               "marca_id": 1,
    *               "created_at": "2022-08-25T22:37:14.000000Z",
    *               "updated_at": "2022-08-25T22:37:14.000000Z",
    *               "marca": {
    *                   "id": 1,
    *                   "nombre": "Samsung",
    *                   "created_at": "2022-08-25T22:37:14.000000Z",
    *                   "updated_at": "2022-08-25T22:37:14.000000Z"
    *               }
    *           },
    *           {
    *               "id": 4,
    *               "nombre": "Galaxy Note 10+",
    *               "marca_id": 1,
    *               "created_at": "2022-08-25T22:37:14.000000Z",
    *               "updated_at": "2022-08-25T22:37:14.000000Z",
    *               "marca": {
    *                   "id": 1,
    *                   "nombre": "Samsung",
    *                   "created_at": "2022-08-25T22:37:14.000000Z",
    *                   "updated_at": "2022-08-25T22:37:14.000000Z"
    *               }
    *           }
    *       ]
    *   }
    */
    public function getModelos($marca_id)
    {
        try {
            $modelos = Modelo::with('marca')->where('marca_id', $marca_id)->get();
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => 'Ocurrió un error al mostar la lista de modelos.',
                'content' => $th->getMessage()
            ], 500);
        }

        return response()->json([
            'success' => true,
            'content' => $modelos
        ], 200);
    }
}
