<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Marca;

class MarcaController extends Controller
{
    /**
    * @api {get} /marcas/get-marcas Obtiene la lista de las marcas registradas
    * @apiName getMarcas
    *
    * @apiSuccessExample Success-Response:
    *     HTTP/1.1 200 OK
    *    {
    *       "success": true,
    *       "content": [
    *           {
    *               "id": 1,
    *               "nombre": "Samsung",
    *               "created_at": "2022-08-25T22:37:14.000000Z",
    *               "updated_at": "2022-08-25T22:37:14.000000Z"
    *           },
    *           {
    *               "id": 2,
    *               "nombre": "Xiaomi",
    *               "created_at": "2022-08-25T22:37:14.000000Z",
    *               "updated_at": "2022-08-25T22:37:14.000000Z"
    *           },
    *           {
    *               "id": 3,
    *               "nombre": "Motorola",
    *               "created_at": "2022-08-25T22:37:14.000000Z",
    *               "updated_at": "2022-08-25T22:37:14.000000Z"
    *           },
    *           {
    *               "id": 4,
    *               "nombre": "Nokia",
    *               "created_at": "2022-08-25T22:37:14.000000Z",
    *               "updated_at": "2022-08-25T22:37:14.000000Z"
    *           },
    *           {
    *               "id": 5,
    *               "nombre": "HP",
    *               "created_at": "2022-08-25T22:37:14.000000Z",
    *               "updated_at": "2022-08-25T22:37:14.000000Z"
    *           }
    *       ]
    *   }
    */
    public function getMarcas()
    {
        try {
            $marcas = Marca::all();
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => 'Ocurrió un error al mostar la lista de las marcas.',
                'content' => $th->getMessage()
            ], 500);
        }

        return response()->json([
            'success' => true,
            'content' => $marcas
        ], 200);
    }
}
