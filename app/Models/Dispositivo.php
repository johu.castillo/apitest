<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Bodega;
use App\Models\Modelo;

class Dispositivo extends Model
{
    use HasFactory;

    protected $table = 'dispositivos';
    protected $fillable = ['sku', 'nombre', 'modelo_id'];

    public function modelo()
    {
        return $this->belongsTo(Modelo::class)->with('marca');
    }

    public function bodegas()
    {
        return $this->belongsToMany(Bodega::class)->as('cantidad')->withPivot('cantidad');
    }
}
