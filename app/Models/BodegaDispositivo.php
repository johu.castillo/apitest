<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BodegaDispositivo extends Model
{
    use HasFactory;

    protected $table = 'bodega_dispositivo';
    protected $fillable = ['cantidad', 'bodega_id', 'dispositivo_id'];
}
