<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Modelo;
use App\Models\Dispositivo;

class Marca extends Model
{
    use HasFactory;

    protected $table = 'marcas';
    protected $fillable = ['nombre'];

    public function modelos()
    {
        return $this->hasMany(Modelo::class);
    }

    public function dispositivos()
    {
        return $this->hasManyThrough(Dispositivo::class, Modelo::class);
    }
}
