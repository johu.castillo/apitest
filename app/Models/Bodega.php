<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Bodega extends Model
{
    use HasFactory;

    protected $table = 'bodegas';
    protected $fillable = ['nombre'];

    public function dispositivos()
    {
        return $this->belongsToMany(Dispositivo::class)->as('cantidad')->withPivot('cantidad');
    }
}
