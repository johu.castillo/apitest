<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bodega_dispositivo', function (Blueprint $table) {
            $table->id();
            $table->integer('cantidad')->nullable();
            $table->foreignId('bodega_id')
                ->nullable()
                ->constrained('bodegas')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('dispositivo_id')
                ->nullable()
                ->constrained('dispositivos')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bodega_dispositivo');
    }
};
