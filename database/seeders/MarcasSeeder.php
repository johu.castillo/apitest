<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Marca;

class MarcasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $marca = Marca::create(['nombre' => 'Samsung']);
        $marca = Marca::create(['nombre' => 'Xiaomi']);
        $marca = Marca::create(['nombre' => 'Motorola']);
        $marca = Marca::create(['nombre' => 'Nokia']);
        $marca = Marca::create(['nombre' => 'HP']);
    }
}
