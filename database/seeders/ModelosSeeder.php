<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Modelo;

class ModelosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modelo = Modelo::create(['nombre' => 'Galaxy S22', 'marca_id' => '1']);
        $modelo = Modelo::create(['nombre' => 'Galaxy S20', 'marca_id' => '1']);
        $modelo = Modelo::create(['nombre' => 'Galaxy A21s', 'marca_id' => '1']);
        $modelo = Modelo::create(['nombre' => 'Galaxy Note 10+', 'marca_id' => '1']);

        $modelo = Modelo::create(['nombre' => 'Mi 11', 'marca_id' => '2']);
        $modelo = Modelo::create(['nombre' => '11T Pro', 'marca_id' => '2']);
        $modelo = Modelo::create(['nombre' => 'POCO F3', 'marca_id' => '2']);
        $modelo = Modelo::create(['nombre' => 'Redmi Note 11', 'marca_id' => '2']);

        $modelo = Modelo::create(['nombre' => 'Edge 30 Pro', 'marca_id' => '3']);
        $modelo = Modelo::create(['nombre' => 'moto g71 5G', 'marca_id' => '3']);
        $modelo = Modelo::create(['nombre' => 'RAZR 5G', 'marca_id' => '3']);
        $modelo = Modelo::create(['nombre' => 'moto G22', 'marca_id' => '3']);

        $modelo = Modelo::create(['nombre' => '808 PureView', 'marca_id' => '4']);
        $modelo = Modelo::create(['nombre' => 'Lumia 800', 'marca_id' => '4']);
        $modelo = Modelo::create(['nombre' => '8 Sirocco', 'marca_id' => '4']);
        $modelo = Modelo::create(['nombre' => '7 Plus', 'marca_id' => '4']);

        $modelo = Modelo::create(['nombre' => 'Stream 14-ax003ns', 'marca_id' => '5']);
        $modelo = Modelo::create(['nombre' => '15-bw007ns', 'marca_id' => '5']);
        $modelo = Modelo::create(['nombre' => 'Pavilion 14-bk001ns', 'marca_id' => '5']);
        $modelo = Modelo::create(['nombre' => 'ENVY 13-ad110ns', 'marca_id' => '5']);
    }
}
