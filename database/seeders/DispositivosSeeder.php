<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Dispositivo;

class DispositivosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrays = range(0, 15);
        foreach ($arrays as $index => $item) {
            Dispositivo::create([
                'sku' => Str::random(8),
                'nombre' => 'Teléfono',
                'modelo_id' => $index + 1
            ]);
        }

        $dispositivo = Dispositivo::create(['sku' => Str::random(8), 'nombre' => 'Laptop', 'modelo_id' => '17']);
        $dispositivo = Dispositivo::create(['sku' => Str::random(8), 'nombre' => 'Laptop', 'modelo_id' => '18']);
        $dispositivo = Dispositivo::create(['sku' => Str::random(8), 'nombre' => 'Laptop', 'modelo_id' => '19']);
        $dispositivo = Dispositivo::create(['sku' => Str::random(8), 'nombre' => 'Laptop', 'modelo_id' => '20']);
    }
}
