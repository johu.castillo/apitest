<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Bodega;
use App\Models\BodegaDispositivo;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Bodega::factory(10)->create();

        $this->call([
            MarcasSeeder::class,
            ModelosSeeder::class,
            DispositivosSeeder::class
        ]);

        BodegaDispositivo::factory(100)->create();
    }
}
