<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\BodegaController;
use App\Http\Controllers\api\MarcaController;
use App\Http\Controllers\api\ModeloController;
use App\Http\Controllers\api\DispositivoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api', 'namespace' => 'api'], function () {
    Route::get('bodegas/get-bodegas', [BodegaController::class, 'getBodegas']);
    Route::get('marcas/get-marcas', [MarcaController::class, 'getMarcas']);
    Route::get('modelos/get-modelos/{marca_id}', [ModeloController::class, 'getModelos']);

    Route::group(['prefix' => 'dispositivos'], function () {
        Route::post('get', [DispositivoController::class, 'getDispositivos']);
        Route::post('disp-marca', [DispositivoController::class, 'getDispositivosByMarca']);
        Route::post('disp-modelo', [DispositivoController::class, 'getDispositivoByModelo']);
        Route::get('disp-bodega/{bodega_id}', [DispositivoController::class, 'getDispositivoByBodega']);
    });
});
